def walk(cmd, val):
    val = int(val)
    # return position, acc
    if cmd == 'nop':
        return 1, 0
    elif cmd == 'acc':
        return 1, val
    elif cmd == 'jmp':
        return val, 0


with open('8_threep.txt', 'r') as f:
    instructions = f.read().splitlines()
    instructions = [x.split() for x in instructions]
history = []

accumulate = 0
position = 0

while True:
    cmd, val = instructions[position]
    history += [position] 
    pos, acc = walk(cmd, val)
    position += pos
    accumulate += acc
    if position in history:
        print("8.1 accumulate = {} before the loop".format(accumulate))
        break


result = []
seen = []
for idx in range(len(instructions)):
    count = 0
    accumulate = 0
    position = 0
    history = []
                
    while True:
        count += 1
        cmd, val = instructions[position]
        val = int(val)
        if (cmd in ('jmp', 'nop') and 
            count not in seen):
            seen.append( count )
            if cmd == 'jmp':
                cmd = 'nop'
            elif cmd == 'nop':
                cmd == 'nop'
        history += [position]
        pos, acc = walk(cmd, val)
        position += pos
        accumulate += acc
        if position in history:
            result.append([(cmd,idx,count)])
            break
         
print(sorted(result))

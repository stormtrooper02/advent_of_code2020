import re
from operator import itemgetter

filename = '5.txt'

def cutinhalf(l: list, high):
    mid = len(l) // 2
    if high:
        return l[mid:]
    else:
        return l[:mid]

result = []

with open(filename, 'r') as f:
    
    for boarding in f.read().splitlines():
        row = range(128)
        seat = range(8)
        
        #replacing lower half with 0 and upper half with 1
        boarding = re.sub(r'[F|L]', r'0', boarding)
        boarding = re.sub(r'[B|R]', r'1', boarding)
        
        #determine row
        for step in boarding[:7]:
            row = cutinhalf(row, int(step))

        #determine seat
        for step in boarding[7:]:
            seat = cutinhalf(seat, int(step))

        result.append(row[0] * 8 + seat[0])

print(f'5.1 The highest ID Number is {sorted(result)[-1]}')

#####################################################################
low, high = itemgetter(0,-1)(sorted(result))
all_seats = set(range(low, high + 1))
my_seat = all_seats.difference( set(result) )

print(f'5.2 My seat ID is {my_seat.pop()}') 
    

file = open('day1_input.txt', 'r')
content = file.read().splitlines()
list_of_numbers = []
for line in content:
    list_of_numbers.append(int(line))
print(list_of_numbers)

length = len(list_of_numbers)
# for i in range(length):
#     for j in range(i + 1,length):
#         if (list_of_numbers[i] + list_of_numbers[j]) == 2020:
#             print(f"Die beiden Zahlen sind {list_of_numbers[i]} und {list_of_numbers[j]}.")
#             print(f"Das Produkt ist {list_of_numbers[i] * list_of_numbers[j]}.")

for i in range(length):
    for j in range(i + 1,length):
        for k in range(j + 1, length):
            if (list_of_numbers[i] + list_of_numbers[j] + list_of_numbers[k]) == 2020:
                print(f"Die drei Zahlen sind {list_of_numbers[i]}, {list_of_numbers[j]} und {list_of_numbers[k]}.")
                print(f"Das Produkt ist {list_of_numbers[i] * list_of_numbers[j] * list_of_numbers[k]}.")


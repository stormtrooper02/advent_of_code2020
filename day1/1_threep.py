filename = '1.txt'

with open(filename, 'r') as f:
    nums = [ int(num.strip()) for num in f.readlines() ]

#1.1

for idx, first in enumerate(nums):
    for second in nums[idx + 1:]:
        if second + first == 2020:
                result = [first, second, first*second]
print(f"1.1 {result[0]} * {result[1]} = {result[2]}")

#1.2

for idx, first in enumerate(nums):
    for idx2, second in enumerate(nums[idx + 1:]):
        for third in nums[idx2 + 1 :]:
            if second + first + third == 2020:
                res = [first, second, third, first * second * third]
print(f"1.2 {res[0]} * {res[1]} * {res[2]} = {res[3]}")


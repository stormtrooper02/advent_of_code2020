import re

filename = '4.txt'

with open(filename) as f:
    passports = f.read().split('\n\n')

# Number of passports where all fields are present and valid respectively
present = 0
validated = 0

expected = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
for passport in passports:
     fields = re.split('\s+', passport.strip())
     dct = dict([re.split(':', x) for x in fields]) 
     test = all([ item in dct.keys() for item in expected ])
     if test:
         present += 1
         byr = int(dct['byr']) in range(1920, 2003)
         iyr = int(dct['iyr']) in range(2010, 2021)
         eyr = int(dct['eyr']) in range(2020, 2031)

         hgt = re.match(r'(\d+)', dct['hgt']).group()
         if 'cm' in dct['hgt']:
            hgt = int(hgt) in range(150, 193)
         else:
            hgt = int(hgt) in range(59, 76)    

         hcl = re.match(r'#[0-9a-f]{6}', dct['hcl'])
         ecl = dct['ecl'] in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']
         pid = re.match(r'\d{9}', dct['pid'])

         if all([byr, iyr, eyr, hcl, ecl, pid, hgt]):
             validated += 1
print("4.1 Number of passports with all reqired fields: {}".format(present))
print("4.2 Number of passports with valid fields: {}".format(validated))

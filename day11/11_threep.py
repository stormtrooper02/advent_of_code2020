

class Seat:
    """Seat with states occupied = 1, free = 0"""
    def __init__(self, state, coord):
        if state == '#':
            self.state = 1
        else:
            self.state = 0

        self.coord = coord



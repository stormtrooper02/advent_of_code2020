import re
from collections import defaultdict


rulebook = defaultdict(dict) 

rules = open(filename, 'r').read().splitlines()
for rule in rules:
    rule = re.split(' bags contain ', rule)
    top = rule[0]
    contained = rule[1].split(',')
    for contain in contained:
        if 'no other bags' in contain:
            rulebook[top] = {} 
            break
        else:
            m = re.search(r'(\d+) (\w+ \w+) bags?', contain)
            num, color = m.groups()
            rulebook[top][color] = int(num)


def get_colors(colors, seen):
    new = [] 
    colors = set(colors) - set(seen)
    if not colors:
        return 'shiny gold' in seen
    for color in colors:
        seen += [color]
        new +=  list(rulebook[color].keys())
    new = list( set(new) - set(seen) )
    return get_colors(new, seen)

count = 0
for color in rulebook.keys():
    if color == 'shiny gold':
        continue
    else:
        if get_colors([color], []):
            count += 1

print('7.1 Number of colors that contain "shiny gold" bags is {}.'.format(count))


# def bagcount(color, count):
#     if not rulebook[color]:
#         return count
#     for color, num in rulebook[color].items():
#         count += num
#         return num * bagcount(color, count)


def bagcount(color):
    count = 1
    for color, num in rulebook[color].items():
        count += num * bagcount(color)
    return count


# def bagcount(colors, seen, count):
#     new = [] 
#     colors = set(colors) - set(seen)
#     if not colors:
#         return 'shiny gold' in seen
#     for color in colors:
#         seen += [color]
#         new +=  list(rulebook[color].keys())
#     new = list( set(new) - set(seen) )
#     return get_colors(new, seen)


print(bagcount('shiny gold') - 1)

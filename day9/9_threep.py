from itertools import accumulate, count, combinations, starmap
from operator import add

filename = '9_threep.txt'

with open(filename, 'r') as f:
    xmas = [ int(_) for _ in f.read().splitlines() ]

start = count(0)

while True:
    s = next(start)
    e = s + 26
    comb = combinations(xmas[s:e], 2)
    calculated = starmap(add, comb)
    currentnumber = xmas[e]

    if currentnumber not in calculated:
        print(currentnumber)
        break
    

start = count(0)
loop = True 

while loop:
    s = next(start)

    for e in range(1,len(xmas) - 1):
        acc = sum(xmas[s:e])
        if acc > currentnumber:
            break
        elif currentnumber == acc:
            loop = False
            print(f"9.2 The encryption weakness is {min(xmas[s:e]) + max(xmas[s:e])}")
            break
    

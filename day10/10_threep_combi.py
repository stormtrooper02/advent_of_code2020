from itertools import tee
from math import prod

filename = '10_ex_2.txt'

def multiwise(iterable, n=2):
    '''takes an iterable as input and returns n copies.
    Each iterable is advanced 0 - n steps e.g.:
    ([1,2,3,4,5,6], n=3) -> [1,2,3], [2,3,4], [3,4,5], [4,5,6]'''
    multi = tee(iterable, n)
    for idx in range(1, n):
        for _ in range(idx):
            next(multi[idx], None)
    return zip(*multi)



with open(filename, 'r') as f:
    jolts = sorted([int(num) for num in f.read().splitlines()])

differences = {3: 1, 1: 1}        # "1:1, 3: 1" for initial, last step
for a,b in multiwise(jolts, 2):
    diff = b-a
    differences[diff] += 1

print(f"10.1 Jolt differences are {prod(differences.values())}\n")

#### collect archipel

device = max(jolts) + 3
island = []
archipel = []
onIsland = 0

def measure(island):
    size = len(island)
    comb = 2 ** size
    if size > 2:
        comb -= 1
    return comb

for a,b,c in multiwise([0] + jolts + [device], 3):
    low = b-a
    high = c-b

    if all([ x < 3 for x in [low, high] ]):    # enter island
        island.append( b )
        onIsland = 1
    
    elif onIsland:         # leave island
        archipel.append(measure(island))
        island = []
        onIsland = 0

print(f"10.2 There are {prod(archipel)} possible combinations for \n\n {jolts}")


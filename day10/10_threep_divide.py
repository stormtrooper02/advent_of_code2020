from itertools import tee, combinations
from math import prod

filename = '10_ex_2.txt'

with open(filename, 'r') as f:
    jolts = sorted([int(num) for num in f.read().splitlines()])

socket = 0
device = max(jolts) + 3

def pairwise(iterable):
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)

def validate(liste):
    for a,b in pairwise(liste):
        if b-a > 3:
            return False
    return True

def generateCombinations(jolts):
    count = 0
    for r in range(20,len(jolts) + 1):
        for combi in combinations(jolts, r):
            count += validate([socket] + list(combi) + [device])
    return count

print(generateCombinations(jolts))




filename = '2.txt'

#2.1

valid = []
with open(filename, 'r') as f:
    for line in f.readlines():
        m = re.search(r'(\d+)-(\d+) (\w): (\w+)', line.strip('\n'))
        low, high, letter, password = m.groups()
        count = Counter(password)
        if count[letter] >= int(low) and count[letter] <= int(high):
            valid.append(password)
    print("2.1 Number of valid passwords: {}".format(len(valid)))
    
#2.2

valid = []
with open(filename, 'r') as f:
    for line in f.readlines():
        m = re.search(r'(\d+)-(\d+) (\w): (\w+)', line.strip('\n'))
        low, high, letter, password = m.groups()
        l1 = password[int(low) - 1]
        l2 = password[int(high) - 1]
        test = [ x == letter for x in [l1, l2] ]
        if not all(test) and any(test):
            valid.append(password)
    print("2.2 Number of valid passwords: {}".format(len(valid)))

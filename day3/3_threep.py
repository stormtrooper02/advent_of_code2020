from itertools import cycle
import math


filename = '3.txt'

#3.1

def read_file(filename):
    with open(filename, 'r') as f:
        return f.read().splitlines()

lines = read_file(filename)

#convert "#" to 1 and "." to 0
def count_trees(right, down, lines):
    slope = []
    start = right + 1
    trees = 0

    for line in lines:
        slope.append( [ 1 if x == '#' else 0 for x in line ] )

    for row, path in enumerate(slope[1:], 1):

        if row % down == 0:

            cyc = cycle(path)
            for idx, item in enumerate(cyc, 1):
                if start == idx:
                    start += right 
                    print(idx, item, path)
                    trees += item
                    break

    return trees

result = []
slopes = [(1,1), (3,1), (5, 1), (7, 1), (1, 2)]
for item in slopes:
    right, down = item
    result.append( count_trees(right, down, lines) )
print( f'Product of Trees{list(zip(slopes, result))} = {math.prod(result)}' )

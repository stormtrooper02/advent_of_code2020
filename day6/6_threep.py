import re

anycount = 0
everycount = 0

with open('6.txt') as f:
    answers = re.split('\n\n', f.read())
    for group in answers:
        yes = set( group )
        yes.discard('\n')
        anycount += len(yes)
        
        persons = group.strip().split('\n')
        intersection = set(persons[0])
        for person in persons[1:]:
            intersection = intersection & set(person)
        everycount += len(intersection)
print("6.1 The number of answers anyone answered is {}".format( anycount ))
print("6.2 The number of answers everyone answered is {}".format( everycount ))
